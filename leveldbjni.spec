%global debug_package %{nil}

Name:          leveldbjni
Version:       1.8
Release:       22
Summary:       A Java Native Interface to LevelDB
License:       BSD
URL:           https://github.com/fusesource/leveldbjni/
Source0:       https://github.com/fusesource/leveldbjni/archive/leveldbjni-%{version}.tar.gz

BuildRequires: gcc-c++ autoconf automake libtool maven-local mvn(junit:junit) mvn(org.apache.felix:maven-bundle-plugin)
BuildRequires: mvn(org.apache.maven.plugins:maven-source-plugin) mvn(org.apache.maven.plugin-tools:maven-plugin-tools-javadoc)
BuildRequires: mvn(org.fusesource:fusesource-pom:pom:) mvn(org.fusesource.hawtjni:hawtjni-runtime) pkgconfig(leveldb) >= 1.7.0-4
BuildRequires: mvn(org.fusesource.hawtjni:maven-hawtjni-plugin) mvn(org.iq80.leveldb:leveldb-api) snappy-devel xmvn

%description
LevelDB JNI provides a Java interface to the LevelDB C ++ library,
which is a fast key-value store written by Google that provides
an ordered mapping from string keys to string values.

%package help
Summary:       Javadoc for leveldbjni
Provides:      %{name}-javadoc = %{version}-%{release}
Obsoletes:     %{name}-javadoc < %{version}-%{release}
BuildArch:     noarch

%description help
This package contains a java manual for introducing leveldbjni.

%prep
%autosetup  -n leveldbjni-leveldbjni-%{version} -p1

%pom_remove_plugin :jxr-maven-plugin
%pom_remove_plugin :surefire-report-maven-plugin
%pom_remove_dep org.fusesource.leveldbjni:leveldbjni-osx leveldbjni-all
%pom_remove_dep org.fusesource.leveldbjni:leveldbjni-win32 leveldbjni-all
%pom_remove_dep org.fusesource.leveldbjni:leveldbjni-win64 leveldbjni-all

%pom_xpath_remove "pom:Private-Package" leveldbjni-all
%pom_xpath_remove "pom:Export-Package" leveldbjni-all
%pom_xpath_remove "pom:Import-Package" leveldbjni-all

%pom_xpath_inject "pom:plugin[pom:artifactId='maven-bundle-plugin']/pom:configuration/pom:instructions" '
<Import-Package>
 org.fusesource.hawtjni.runtime,
 org.iq80.leveldb*;version=${leveldb-api-version}
</Import-Package>' leveldbjni-all

%pom_xpath_remove "pom:Bundle-NativeCode"  leveldbjni-all

%pom_remove_dep org.fusesource.leveldbjni:leveldbjni-linux32 leveldbjni-all

%pom_xpath_inject "pom:plugin[pom:artifactId='maven-bundle-plugin']/pom:configuration/pom:instructions" "
<Bundle-NativeCode>
  META-INF/native/linux64/libleveldbjni.so;osname=Linux;processor=x86-64
</Bundle-NativeCode>" leveldbjni-all

find leveldbjni/src/test/java/org/fusesource/leveldbjni/test -name DBTest.java -delete
cp -f /usr/lib/rpm/config.{sub,guess} /usr/share/automake-*/compile leveldbjni/src/main/native-package/autotools/

%pom_remove_plugin :maven-jar-plugin leveldbjni
%pom_add_plugin org.apache.maven.plugins:maven-jar-plugin:3.0.2 leveldbjni '
<configuration>
  <archive>
    <manifestFile>${project.build.outputDirectory}/META-INF/MANIFEST.MF</manifestFile>
  </archive>
</configuration>'

%build

%mvn_file :leveldbjni-all leveldbjni-all
%mvn_file :leveldbjni-linux64 leveldbjni-linux
export JAVA_HOME=%{_jvmdir}/java LEVELDB_HOME=%{_prefix} SNAPPY_HOME=%{_prefix}
sed -i '43c CFLAGS="$CFLAGS -I${withval}/include -Wl,-s"' leveldbjni/src/main/native-package/m4/custom.m4
sed -i '44c CXXFLAGS="$CXXFLAGS -I${withval}/include -Wl,-s"' leveldbjni/src/main/native-package/m4/custom.m4
%mvn_build -- -Plinux64,all -Dleveldb=%{_prefix} -Dsnappy=%{_prefix}

%install
%mvn_install

%files  -f .mfiles
%doc changelog.md releasing.md
%license license.txt

%files help -f .mfiles-javadoc
%doc readme.md

%changelog
* Wed Mar 15 2023 yaoxin <yaoxin30@h-partners.com> - 1.8-22
- Fix build failure

* Tue Mar 07 2023 wulei <wulei80@h-partners.com> - 1.8-21
- Fix not stripped problem

* Fri Dec 20 2019 yangjian<yangjian79@huawei.com> - 1.8-20
- Package init
